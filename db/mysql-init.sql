use FulfillGriffin;

DROP TABLE IF EXISTS `FulfillGriffin`;
CREATE TABLE `FulfillGriffin` (
  `GriffinId`	varchar(255) NOT NULL,
  `DeptNo`		bigint(20),
  PRIMARY KEY (`GriffinId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO FulfillGriffin VALUES ('Sharealot', NULL);
INSERT INTO FulfillGriffin VALUES ('Lovealot', NULL);
INSERT INTO FulfillGriffin VALUES ('Lancelot', NULL);
INSERT INTO FulfillGriffin VALUES ('Devo', NULL);
INSERT INTO FulfillGriffin VALUES ('Romeo', NULL);
INSERT INTO FulfillGriffin VALUES ('Dreamboat', NULL);
INSERT INTO FulfillGriffin VALUES ('Stargirl', NULL);
INSERT INTO FulfillGriffin VALUES ('MrRobot', NULL);
INSERT INTO FulfillGriffin VALUES ('SirDavid', NULL);
INSERT INTO FulfillGriffin VALUES ('Buster', NULL);
