# GetAGriffin-FulfillGriffin Component (Microservice)

This is a component level repo for the sample product used in the O'Reilly/Pearson online course,
*More Effective DevOps Testing*. Please see the main (product level) repo for more information:
https://gitlab.com/cliffbdf/getagriffin

## Other Repos Needed

The following repo is needed for this product. It is not in Maven Central, so you will need
to clone it and build it, by running `make install`, which will build it and install it
in your local maven repository:

https://gitlab.com/cliffbdf/utilities-java

## To Build This Component

```
# Set env variables that the scripts below need
source env.vm.source

# build in Linux to create the container image
./build.sh
```

## To Deploy This Component

### Locally (in Linux, e.g., a local Linux VM)

```
source env.vm.source
./deploy.vm.sh
```

### In AWS (using CDK)

```
./deploy.cdk.sh
```

## To Run Test Suites

### Locally (in Linux, e.g., a local Linux VM)

```
source env.vm.source
./comptest.sh
```

### Under Jenkins in AWS

TBD

## To Run Test Suites

After deploying for test, one may run the test suites:

### Locally

```
# Run the Cucumber tests, which make calls to the SUT
./comptest.sh

# Clean up - remove deployment
./destroy.vm.sh
```

### Under Jenkins

TBD

## Component Level Test Strategy

1. API test

## External Packages used

The microservices in this product use the SparkJava framework, which is a much
lighterweight, less opinionated alternative to Spring. It is also easier for those
new to the framework to understand the code. The documentation for SparkJava
can be found here: http://sparkjava.com/documentation

## The Service's REST api

RequisitionFromInventory?DeptNo=

AddToInventory?GriffinId=

CancelRequisition?GriffinId=

GetGriffinsAvailCount
